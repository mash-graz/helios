#!/usr/bin/env python3

## to test the package without system wide installation
import sys, os, math
sys.path.append(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))

import helios, time
import numpy as np

red = (255,0,0, 255)
blue = (0,0,255, 255)
green= (0,255,0, 255)
black = (0,0,0,0)

# anzahl der berechneten sinnuskurvenpunkte
density = 100

fl = helios.FrameSeq()
for n in range(200):

    # neuer frame
    f = helios.Frame()

    # berechnen einer sinuskurve
    ramp = np.linspace(-1,1,density)
    arr = np.zeros(density, f.fields_float)
    arr['x'] = ramp *.5
    arr['y'] = np.sin((ramp + n/100.0) * math.pi) *.5
    arr['b'][:] = 255
    arr['i'][:] = 255

    # blank am anfang
    f.append_float([(0,0, *black),(-.5,arr[0]['y'], *black)], nr=100)
                                     # ^^^^^^^^^^^ erster punkt   
    
    # hinzufügen der berechneten kurvenpunkte
    f.append_float(arr)

    # blank am ende -- hier ist es weniger kritisch
    f.append_float([(.5, arr[-1]['y'], *black), (0,0, *black)], nr=20)
                      #  ^^^^^^^^^^^^ letzter punkt

    # anfügen des frames an die frame sequenz
    fl.append(f)

print("write APNG...")
fl.write_apng("example-2.png")
print("start laser...")
fl.write_helios(pps=15000)
