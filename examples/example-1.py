#!/usr/bin/env python3

## to test the package without system wide installation
import sys, os
sys.path.append(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))

import helios, time

# die farb- und intensitätswerte für die punkte in dieser weise
# vorzubereiten, hat den vorteil, das man sie mit dem python 
# unpacking operator als bspw. '*red' später einfügen kann.
red = (255,0,0, 255)
blue = (0,0,255, 255)
green= (0,255,0, 255)
black = (0,0,0, 0)

# intitialisieren der gerätetreiber klasse
h = helios.Helios()

# neuer frame
f = helios.Frame()

# hinzufügen von punkten bzw. punktsequenzen
f.append_float([(0,0,*black),(-1,1,*black)],nr=10)

# diesr befehl zeichnet eine schwarze line von 
# 0,0 nach -1,1
#
# diese koordinatenangabenen in der fließkommaversion beziehen
# sich auf folgendes schema:
#
#          +1 y
#           |
#   -1 x -- 0 -- +1 x
#           |
#          -1 y
#
# es wird hier auch von der option 'nr' gebrauch gemacht, die 
# zwischen den nagegebenen punkten auch noch weitere interpolierte
# einfügt, so dass am ende die angebene anzahl an punkten gesetzt wird.
#
# die schwarzen linen zwischen unzusammenhängenden hellen strichen 
# im bild sind unbedingt nötig, weil sonst die spiegel nicht schnell 
# genug an den anfang der linie gelenkt werden und ausgesprochen 
# unhübsche verzerrungen auftreten.

# neben der fließkomma-variante, kann man die koordianten auch in der 
# ganzzahligen form angeben, wie sie das interface tatsächlich nutzt:
f.append([(0,0, *red),(0xfff,0xfff, *red)], nr=20)

# hier schaut das koordinatensystem folgendernmaßen aus:
# 
#                    0xfff y
#                      |
#                      |
#   0xfff x -----------0
#

f.append_float([(-1,1,*black),(-.5,-1,*black)],nr=10)
f.append_float([(-.5, -1, *blue), (.5, .5, *blue)],nr=10)
f.append_float([(.5,.5,*black),(1,0,*black)],nr=10)
f.append_float([(1, 0, *green), (-.5, 0, *green)],nr=10)
f.append_float([(-.5,0,*black),(0,0,*black)],nr=10)

# ausgabe des frames als simulierte bilddatei
# die option show, zeigt dfas ergebenis auch am bildschirm
f.as_image('example-1.png', show=True)

# ausgabe über den DAC
h.write_frame(f, pps=6000)

# obwohl ein frame im defaultmodus automatisch wiederholt wird, 
# bis ein en neuer frame gezeigt werden soll, ist hier noch eine 
# abschließende pause nötig, da das programm sonst beendet würde, 
# und damit auch die ausgabe über den laser projektor.
time.sleep(100) 