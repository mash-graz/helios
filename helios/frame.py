import logging, sys, os, io, time
from typing import *
import numpy as np
import aggdraw
from scipy import interpolate
from PIL import Image
import apng
from . import helios

class Frame:
    """Single laser projection frame"""
    fields = [('x', 'u2'),('y','u2'),('r','u1'),('g','u1'),('b','u1'),('i','u1')]
    fields_float = [('x', 'f4'),('y','f4'),('r','u1'),('g','u1'),('b','u1'),('i','u1')]

    def __init__(self, points: [Tuple,List]=None):
        """Singe Frame of laser projection points.

        This class constructor can be used without arguments.

        If an optional list of points is given, it has to use
        native Helios coordinates. 
        
        Keyword Arguments:
            points {[type]} -- [description] (default: {None})
        """
        if points:
            self.points = np.array(np.array(points, dtype=self.fields))
        else:
            self.points = np.array([], dtype=self.fields)

    def __len__(self) -> int:
        """Number of points in this frame.
        
        Returns:
            int -- number of points
        """
        return len(self.points)

    def append(self, points: [Tuple,List], nr: int=0, k: int=1):
        """Append point[s] to frame.

        This Variant requires native Helios coordinates.

        If 'nr' and 'k' are specified, additional interpolated 
        points will be added.
        
        Arguments:
            points -- single point tuple or List of point tuples 
                      using Helios coordinates 
                      i.e. (x, y, r, g, b, i)
        
        Keyword Arguments:
            nr {int} -- nr. of points after interpolation (default: {0})
            k {int} -- order of interpolation 1:linear, 3:cubic (default: {1})
        """
        tmp = np.array(points, dtype=self.fields)
        if nr:
            tmp = self.interpolate(tmp, nr, k)
        self.points = np.append(self.points, tmp)

    def append_float(self, points: [Tuple,List], nr:int=0, k: int=1):
        """Append point[s] to frame.

        This Variant utilizes floting point coordinates.

                   +1 y
                    |
            -1 x -- 0 -- +1 x
                    |
                   -1 y

        If 'nr' and 'k' are specified, additional interpolated 
        points will be added.
        
        Arguments:
            points -- single point tuple or list of point tuples 
                      using Helios coordinates 
                      i.e. (x, y, r, g, b, i)
        
        Keyword Arguments:
            nr {int} -- nr. of points after interpolation (default: {0})
            k {int} -- order of interpolation 1:linear, 3:cubic (default: {1})
        """
        half = helios.MAX_XY / 2
        f = np.array(points, dtype=self.fields_float)
        f['x'] = half - f['x'] * half  # mirror only x-axis
        f['y'] = half - f['y'] * -half  
        if nr:
            f = self.interpolate(f, nr, k)
        self.points = np.append(self.points, f.astype(self.fields))

    def interpolate(self, p, nr: int, k: int=1) -> List:
        """Expands a list of points by adding interpolated ones
        
        Arguments:
            p {[type]} -- point list
            nr {int} -- nr. of points after interpolation
        
        Keyword Arguments:
            k {int} -- order of interpolation (1:linear, 3:cubic)
        
        Returns:
            [type] -- list of points
        """
        tckp,u = interpolate.splprep([p['x'],p['y'],p['r'],p['g'],p['b'],p['i']],k=k)
        out = np.zeros(nr, dtype=self.fields)
        out['x'],out['y'],out['r'],out['g'],out['b'],out['i'] = interpolate.splev(np.linspace(0,1,nr), tckp)
        return out

    def as_image(self, path=None, size=(512,512), show=False)->io.BytesIO:
        """Draws an image of frame content
        
        if a 'path' is given, a file will be written.

        Keyword Arguments:
            path {[type]} -- filename (default: {None})
            size {tuple} -- image size (default: {(512,512)})
            show {bool} -- display on screen (default: {False})
        
        Returns:
            [String] -- an io.BytesIO object of the image, 
                        if '-' was specified as path.
        """
        img = Image.new('RGB', size)
        dc = aggdraw.Draw(img)

        # translate to img coordinates
        img_points = self.points.astype(self.fields_float)
        img_points['x'] = size[0] - (img_points['x'] / helios.MAX_XY * size[0])
        img_points['y'] = size[1] - (img_points['y'] / helios.MAX_XY * size[1])

        color = (0,0,0)
        pen = aggdraw.Pen(color)
        for n in range(len(img_points)-1):
            color_now = tuple(img_points[n][['r','g','b']])
            if color_now != color:
                color = color_now
                pen = aggdraw.Pen(color, opacity=img_points[n]['i'], width=3)

            logging.debug(f"c: {color_now}, cords: {(img_points[n]['x'], img_points[n]['y'])} -> {(img_points[n+1]['x'], img_points[n+1]['y'])}")
            dc.line((img_points[n]['x'], img_points[n]['y'],
                    img_points[n+1]['x'], img_points[n+1]['y']), pen)
        dc.flush()

        if show:
            img.show()

        if not path:
            return
        elif path == '-':
            b_io = io.BytesIO()
            img.save(b_io, 'PNG')
            return b_io
        else:
            img.save(path)

class FrameSeq:

    def __init__(self, frames = None):
        """New Frame Sequence.

        optionaly adding a frame or frame list
        
        Keyword Arguments:
            frames {[type]} -- frames to add (default: {None})
        """
        if isinstance(frames, Frame):
            self.frames = [frames]
        elif isinstance(frames, list):
            self.frames = frames
        else:
            self.frames = []

    def append(self, frames):
        """Append a frame or frame list
        
        Arguments:
            frames {[type]} -- frames to add
        """
        if isinstance(frames, Frame):
            self.frames.append(frames)
        elif isinstance(frames, list):
            self.frames += frames

    def write_apng(self, filename, size=(512,512), 
                pps: int=30000, delay: float=0):
        """Write a APNG simulation of the animation
        
        Arguments:
            filename
        
        Keyword Arguments:
            size {tuple} -- image size (default: {(512,512)})
            pps {int} -- scann speed (default: {30000})
            delay {float} -- additional delay per frame (default: {0})
        """
        png = apng.APNG()
        for f in self.frames:
            t = int((len(f) / pps + delay) * 100)
            png.append_file(f.as_image(path='-', size=size), delay=t)
        png.save(filename)

    def write_helios(self, h=None, dev_nr: int = 0, 
                    pps: int=30000,  flags: int = 0,
                    delay: float=0):
        """Writes animation via DAC
        
        Keyword Arguments:
            h {[type]} -- helios class (default: {None})
            dev_nr {int} -- device nr. (default: {0})
            pps {int} -- scann speed (default: {30000})
            flags {int} -- helios write flags (default: {0})
            delay {float} -- additional delay per frame (default: {0})
        """
        if not h:
            h = helios.Helios()
        while True:
            for f in self.frames:
                if not h.get_status():
                    logging.debug('Helios status: "not ready"')
                h.write_frame(f, dev_nr, pps, flags)
                if delay:
                    frame_t = len(f) / pps
                    time.sleep(max(0, delay - frame_t))
