#!/usr/bin/env python3
"""Helios DAC laser projection control"""

import logging, sys, os, ctypes
from typing import *
import numpy as np
from . import frame

MAX_XY = 0xFFF

MAX_POINTS = 0x1000
MAX_RATE   = 0xFFFF
MIN_RATE   = 7

DEFAULT           = 0
START_IMMEDIATELY = 1
SINGLE_MODE       = (1 << 1)
DONT_BLOCK        = (1 << 2)

SUCCESS =  1
ERROR   = -1

# unless otherwise specified, functions return SUCCESS if OK, 
# and ERROR if something went wrong.
HeliosReturnCode = int

class Helios:
    """Helios DAC Hardware control class
    
    Most of the class methodes are just python wrappers around functions 
    in the Helios SDK:

    https://github.com/Grix/helios_dac/blob/master/sdk/HeliosDacAPI.h
    """

    def __init__(self):
        """Loads low level dynamic libraries and initializes all devices."""
        if sys.platform == 'linux':
            self.api = ctypes.cdll.LoadLibrary(os.path.join(
                os.path.dirname(__file__), "libHeliosDacAPI.so"))
        elif sys.platform == 'win32':
            self.api = ctypes.cdll.LoadLibrary(os.path.join(
                os.path.dirname(__file__), "libHeliosDacAPI.so"))
        else:
            logging.error(f"unsupported platform: {sys.platform}")
            sys.exit()
        self.open_devices()


    def __del__(self):
        self.close_devices()

    def open_devices(self) -> int:
        """Initializes the driver and opens connection to all devices.

        Usually this was allready done during class initialization.

        NB You first have to call close(), if you want to re-scan and 
        re-initiate all devices again!
        
        Returns:
            int -- number of found devices
        """
        self.num_devices = self.api.OpenDevices()
        if self.num_devices == 0:
            logging.warning("couldn't connect to physical devices")
        else:
            logging.info(f"found {self.num_devices} devices")
        return self.num_devices

    def close_devices(self) -> HeliosReturnCode:
        """Closes and frees all devices.
        
        Returns:
            HeliosReturnCode -- SUCCESS or ERROR
        """
        ret = self.api.CloseDevices()
        if self.num_devices and (ret == ERROR):
            logging.error("CloseDevices returned error code")
        self.num_devices = 0
        return ret

    def get_status(self, dev_nr: int = 0) -> bool:
        """Check if DAC is ready
        
        Keyword Arguments:
            dev_nr {int} -- device number (default: {0})
        
        Returns:
            bool -- {True} for ready, {False} otherwise
        """
        if self.api.GetStatus(dev_nr) == 1:
            return True
        else:
            return False

    def get_firmware_version(self, dev_nr: int = 0) -> int:
        """Returns firmware version of DAC
        
        Keyword Arguments:
            dev_nr {int} -- device number (default: {0})
        
        Returns:
            int -- firmware version
        """
        return self.api.GetFirmwareVersion(dev_nr)

    def stop(self, dev_nr: int = 0) -> HeliosReturnCode:
        """Stops output of DAC until new frame is written (NB: blocks for 100ms)

        Keyword Arguments:
            dev_nr {int} -- device number (default: {0})
        
        Returns:
            HeliosReturnCode -- SUCCESS or ERROR
        """
        return self.api.Stop(dev_nr)

    def set_shutter(self, dev_nr: int = 0, level: bool=False) -> HeliosReturnCode:
        """Sets shutter level of DAC
        
        Keyword Arguments:
            dev_nr {int} -- device number (default: {0})
            level {bool} -- shutter level (default: {False})
        
        Returns:
            HeliosReturnCode -- SUCCESS or ERROR
        """
        return self.api.SetShutter(dev_nr, level)

    def write_frame(self, frame: Type[frame.Frame], dev_nr: int = 0,
                    pps: int=30000,  flags: int = DEFAULT) -> HeliosReturnCode:
        """
        write frame
        pps:   rate of output in points per second
        flags:  
          START_IMMEDIATELY: 
          start output immediately, instead of waiting 
          for current frame (if there is one) to finish playing

          SINGLE_MODE:
          play frame only once, instead of repeating until another frame is written

          DONT_BLOCK:
          don't let WriteFrame() block execution while waiting for the transfer to finish
        """
        if len(frame) > MAX_POINTS:
            logging.error(f'frame size ({len(frame)}) exceeds MAX_POINTS in write_frame')
            return ERROR
        return self.api.WriteFrame(dev_nr, pps, flags, frame.points.ctypes, len(frame))
