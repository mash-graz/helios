# Helios DAC Python Interface

![beispielanimation](examples/example-2.png)

## Vorbereitungen und Installation

Um diese Python-Erweiterung zur Steuerung des [Helios DAC](http://pages.bitlasers.com/helios/) nutzen zu können, sind folgende Schritte nötig:

* Unter Linux muss mittels einer `udev`-Regel erlaubt werden, dass auch nicht-priviligierte Benutzer Zugriff auf die betreffende Hardware-Schnittstelle erhalten. Das kann bspw. über das Anlegen des Files `/etc/udev/rules.d/50-helios-dac.rules` mit folgendem Inhalt geschehen:

      SUBSYSTEMS=="usb", ATTRS{product}=="Helios Laser DAC", GROUP="plugdev"

* Die Python-Erweiterung selbst kann man u.a. mit folgendem Befehl installieren:

      python3 -m pip install git+https://gitlab.com/mash-graz/helios.git#egg=repoName

  Im Packet enthalten sind fertig kompilierte DLLs aus dem [Helios SDK](https://github.com/Grix/helios_dac), die auf den meisten 64bit Linux und Windows Installationen funktionieren sollten.

* Einige weitere benötigte Python-Abhängtigkeiten wurden bewusst nicht in die Python-Paket-Abhängigkeiten aufgenommen, da sie oft auf anderem Weg sinnvoller installiert werden können. Das betrifft: `numpy, aggdraw, Pillow, scipy, apng`
  
  Unter Debian Linux ist das bspw. so umzusetzten:

      sudo aptitude install python3-numpy python3-pillow python3-scipy

      python3 -m pip install apng aggdraw
