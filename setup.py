from setuptools import setup, find_packages

setup(
    name='helios',
    packages=find_packages(exclude=['examples*']),
    version_format='{tag}.dev{commitcount}+{gitsha}',
    setup_requires=['setuptools-git-version'],
    license='GPL3',
    description="Helios DAC Python Interface",
    url='https://gitlab.com/mash-graz/helios',
    install_requires=[],
    author='Martin Schitter',
    author_email='ms+helios@mur.at'
)